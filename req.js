export const request = (po={})=>{
	return new Promise((reslove,rej)=>{
		uni.request({
			url:'http://124.93.196.45:10001'+po.url,
			method:po.method||'GET',
			header:po.header,
			data:po.data||{},
			success: (res) => {
				if(res.data.code==200){
					reslove(res.data)
					console.log(res.data)
				}else{
					rej(res)
					uni.showToast({
						title:res.data.msg,
						icon:'error'
					})
					console.log(res)
				}
			},
			file:(err)=>{
				uni.showToast({
					title:"网络连接失败",
					icon:"error"
				});
				rej(err)
			}

		})
	})
}